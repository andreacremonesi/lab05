note
	description: "Summary description for {GAME_TEST_SET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

frozen class
	GAME_TEST_SET
inherit
	EQA_TEST_SET




			--INSTANTIATE GAME CLASS






		feature rollMany(n : INTEGER;pins : INTEGER;game : GAME)
			--ROLL pins n times

				local
					i : INTEGER

				do

				from i:=0
				until i>=n
				loop
					game.roll(pins)
					i:=i+1
				end
			end
		feature rollSpare(game : GAME)
			--SPARE FOR PLAYER
			do
				rollMany(2,5,game)
			end
		feature rollStrike(game : GAME)
			--STRIKE FOR PLAYER
			do
				game.roll(10)
			end


		feature
		testGutterGame
			--check true for a gutter game
			local
				game : GAME
			do
				create game.make
				rollMany(20,0,game)

				assert("gutter",game.score.is_equal(0))
			end
		feature
		testAllOnes
			--check true for all one games
			local
				game : GAME
			do
				create game.make
				rollMany(20,1,game)
				assert("ones",game.score.is_equal(20))
			end
		feature
		testOneSpare
			local
				game : GAME
			do
				create game.make
				rollSpare(game)
				game.roll(3)
				rollMany(17,0,game)
				assert("oneSpare",game.score.is_equal(16))
			end
		feature
		testOneStrike
			local
				game : GAME
			do
				create game.make
				rollStrike(game)
				game.roll(3);
       			game.roll(4);
        		rollMany(16, 0,game);
        		assert("oneStrike",game.score.is_equal(24))
			end
		feature
		testPerfectGame
			local
				game : GAME
			do
				create game.make
				rollMany(12,10,game)
        		assert("allStrike",game.score.is_equal(300))
			end
		feature
		testLastSpare
			local
				game : GAME
			do
				create game.make
				rollMany(9,10,game);
       			rollSpare(game)
        		game.roll(10)
       			assert("lastSpare",game.score.is_equal(275))
			end
end
