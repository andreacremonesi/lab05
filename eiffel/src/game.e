note
	description: "bowling application root class"
	date: "$Date$"
	revision: "$Revision$"
	EIS: "name=Unnamed", "protocol=URI", "src=$"

class
	GAME

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	points : INTEGER
	first : INTEGER
	first_shot : BOOLEAN
	spare_flag : BOOLEAN
	strike_flag : BOOLEAN
	strike_row : BOOLEAN
	make
			-- Run application.
		do
			--| Add your code here
			points:=0
			first:=0
			first_shot:=true
			spare_flag:=false
			strike_row:=false
		end

feature roll (pins : INTEGER)
	do
		-- first shoot
		if first_shot = true
			then
				if strike_flag=false and spare_flag=false
					then
						if pins=10
						 	then
						 		strike_flag:=true

						else
							first:=pins
							first_shot:=false
						end
				else
					if strike_flag=true
						then
							if pins=10 then
								if strike_row=true then
									points:=points+30
								end
								strike_flag:=true
								strike_row:=true
							else
								if strike_row=true then
									points:=points+20+pins
								end
								first:=pins
								first_shot:=false
							end
					else
						if spare_flag=true then
							if pins=10 then
								strike_flag:=true
							else
								first:=pins
								first_shot:=false
							end
							points:=10+points+pins
						end
					end
				end
				spare_flag:=false
		-- second shoot
		else
			if strike_flag=true

				then
					points:=10+points+pins+first
			end
			if first+pins=10
				then
					spare_flag:=true
				else

				points:=points+first+pins
			end
		first:=0
		first_shot:=true
		strike_flag:=false
		end

	end
feature score : INTEGER
	do
		Result:=points
	end
end
